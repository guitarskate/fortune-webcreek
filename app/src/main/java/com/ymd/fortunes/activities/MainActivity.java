package com.ymd.fortunes.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.tylerjroach.eventsource.EventSource;
import com.tylerjroach.eventsource.EventSourceHandler;
import com.tylerjroach.eventsource.MessageEvent;
import com.ymd.fortunes.R;
import com.ymd.fortunes.retrofit.ApiServiceManager;
import com.ymd.fortunes.utilities.Utils;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    private EventSource eventSource;
    private TextView txtResponseService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
    }

    private void initUI(){
        txtResponseService = findViewById(R.id.txt_response_service);

        findViewById(R.id.btn_start_service).setOnClickListener(v -> startEventSource());
        findViewById(R.id.btn_cancel_service).setOnClickListener(v -> stopEventSource());
        findViewById(R.id.btn_error_service).setOnClickListener(v -> System.out.println("service error"));
    }

    private void getFortuneStream(){
        ApiServiceManager.fortunesStream().subscribe(new SingleObserver<String>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(String accountApi) {
                System.out.println("MESSAGE" + accountApi);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }
        });
    }

    private SSEHandler sseHandler = new SSEHandler();

    private void startEventSource() {
        eventSource = new EventSource.Builder(Utils.API_URL + "fortunes/stream")
                .eventHandler(sseHandler)
                .build();
        eventSource.connect();
    }

    private void stopEventSource() {
        if (eventSource != null)
            eventSource.close();
        sseHandler = null;
    }

    /**
     * All callbacks are currently returned on executor thread.
     * If you want to update the ui from a callback, make sure to post to main thread
     */

    //https://github.com/tylerjroach/eventsource-android
    private class SSEHandler implements EventSourceHandler {

        public SSEHandler() {
        }

        @Override
        public void onConnect() {
            Log.v("SSE Connected", "True");
        }

        @Override
        public void onMessage(String event, MessageEvent message) {
            Log.v("SSE event", event);
            //Log.v("SSE lastEventId: ", message.lastEventId);
            Log.v("SSE data: ", message.data);
            txtResponseService.setText(message.data);
        }

        @Override
        public void onComment(String comment) {
            //comments only received if exposeComments turned on
            Log.v("SSE Comment", comment);
        }

        @Override
        public void onError(Throwable t) {
            //ignore ssl NPE on eventSource.close()
        }

        @Override
        public void onClosed(boolean willReconnect) {
            Log.v("SSE Closed", "reconnect? " + willReconnect);
        }
    }
}
