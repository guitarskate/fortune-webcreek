package com.ymd.fortunes;

import android.app.Application;
import android.content.Context;

public class FortunesApplication extends Application {

    private static FortunesApplication instance;

    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Context getAppContext() {
        return instance;
    }
}