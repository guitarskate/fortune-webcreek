package com.ymd.fortunes.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ymd.fortunes.retrofit.endpoint.ApiEndpoint;
import com.ymd.fortunes.retrofit.interceptor.LoggingInterceptor;
import com.ymd.fortunes.retrofit.interceptor.RequestInterceptor;
import com.ymd.fortunes.utilities.Utils;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiServiceManager {

    private static Retrofit restClient;

    private static ApiServiceManager instance = null;

    private ApiServiceManager() {
        Gson gson = new GsonBuilder()
                .create();

        int REQUEST_TIMEOUT = 10;

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .addNetworkInterceptor(new LoggingInterceptor())
                .addInterceptor(new RequestInterceptor())
                .cache(null)
                .build();

        restClient = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Utils.API_URL)
                .build();
    }

    private static synchronized void getInstance(){
        if(null == instance){
            instance = new ApiServiceManager();
        }
    }

    public static Single<String> fortunesStream() {
        getInstance();
        return restClient.create(ApiEndpoint.class)
                .fortunesStream()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
