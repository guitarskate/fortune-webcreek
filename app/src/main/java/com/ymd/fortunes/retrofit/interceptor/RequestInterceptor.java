package com.ymd.fortunes.retrofit.interceptor;

import com.ymd.fortunes.utilities.AppPreferences;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@SuppressWarnings({"NullableProblems", "AccessStaticViaInstance"})
public class RequestInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();

        request = request.newBuilder()
                .addHeader("Content-Type", "application/json")
                .build();

        String accessToken = AppPreferences.getInstance().getAccessToken();
        if(!accessToken.isEmpty()){
            request = request.newBuilder()
                    .addHeader("Token-Auth", accessToken)
                    .build();
        }

        return chain.proceed(request);
    }

}
