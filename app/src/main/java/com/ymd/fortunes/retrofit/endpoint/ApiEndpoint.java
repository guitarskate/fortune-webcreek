package com.ymd.fortunes.retrofit.endpoint;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Streaming;

public interface ApiEndpoint {

    @GET("fortunes/stream")
    @Streaming
    Single<String> fortunesStream();

}
