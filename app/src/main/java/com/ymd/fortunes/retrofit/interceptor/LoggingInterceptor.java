package com.ymd.fortunes.retrofit.interceptor;

import android.annotation.SuppressLint;

import java.io.IOException;
import java.util.logging.Logger;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

// https://github.com/square/okhttp/wiki/Interceptors
// http://www.vogella.com/tutorials/Logging/article.html

@SuppressWarnings("NullableProblems")
public class LoggingInterceptor implements Interceptor {

    @SuppressLint("DefaultLocale")
    @Override public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        long t1 = System.nanoTime();
        Logger.getAnonymousLogger().info(
                String.format("Sending request %s with method %s on %s%n%s",
                        request.url(), request.method(), chain.connection(), request.headers()));

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        Logger.getAnonymousLogger().info(
                String.format("Received response for %s in %.1fms%n%s",
                        response.request().url(), (t2 - t1) / 1e6d, response.headers()));

        Logger.getAnonymousLogger().info(
                String.format("RESPONSE = %s ",
                        response.message()));

        return response;
    }
}
