package com.ymd.fortunes.utilities;

import com.ymd.fortunes.FortunesApplication;

public class AppPreferences {

    private static AppPreferences instance;

    private AppPreferences(){}

    public static synchronized AppPreferences getInstance(){
        if(instance == null){
            instance = new AppPreferences();
        }
        return instance;
    }

    public static String getAccessToken(){
        return  Preferences.getPreferenceS(FortunesApplication.getAppContext(), Utils.TAG_TOKEN);
    }

}
