package com.ymd.fortunes.utilities;

import android.app.Activity;
import android.content.Intent;

import com.ymd.fortunes.R;

public class Utils {

    public static final int DELAY_SPLASH_SCREEN = 3000;
    public static final String TAG_TOKEN = "token";

    public static final String API_URL = "https://secure.modinter.com.ec:8586/";

    /**
     * funcion que cambia de actividad y realiza una animacion a la actividad
     * @param context - en donde se ejecuta
     * @param activity - clase a la que desea ir
     * @param close - decide si cerrar o no la actividad actual
     */
    public static void animRightLeft(Activity context, Class activity, boolean close){
        context.startActivity(new Intent(context, activity));
        context.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        if(close)
            context.finish();
    }
}
