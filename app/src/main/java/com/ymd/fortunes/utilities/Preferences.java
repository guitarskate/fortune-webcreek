package com.ymd.fortunes.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Yulian Martinez on 30/05/2017.
 * @author Yulian Martinez
 *
 * Permite crear los SharedPrefences de la aplicacion
 * y los metodos correspondientes para agregar y obtener cada dato guardado.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class Preferences {

    private static final String PREFERENCE_N = "PrefFortuneN"; //Prefencia que se usa para datos de la app
    private static final String PREFERENCE_R = "PrefFortuneR"; //Prefencia que se usa para datos del registro
    private static final String PREFERENCE_A = "PrefFortuneA"; //Prefencia que se usa para datos adicionales
    private static final String PREFERENCE_F = "PrefFortuneF"; //Prefencia que se usa para datos de Firebase
    private static final String PREFERENCE_T = "PrefFortuneT"; //Prefencia que se usa para datos temporales
    private static final String JSON_DATA = "Yampi";
    public static final int TYPE_STANDARD = 0;
    public static final int TYPE_REGISTER = 1;
    public static final int TYPE_ADITIONAL = 2;
    public static final int TYPE_FIREBASE = 3;
    public static final int TYPE_TEMPORAL = 4;

    /**
     * Guarda el valor de una preferencia de cadena
     * @param context - en donde se ejecuta
     * @param type - Tipo de preferencia que se va a usar
     * @param key - identificador de la prefencia
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setPreferenceS(Context context, int type,String key, String value){
        SharedPreferences sharedPreferences = getPreference(context, type);
        sharedPreferences.edit().putString(key, value).apply();
    }

    /**
     * Guarda el valor de una preferencia de cadena
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setPreferenceS(Context context, String key, String value){
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCE_N, context);
        sharedPreferences.edit().putString(key, value).apply();
    }

    /**
     * Guarda el valor de una preferencia de numeros
     * @param context - en donde se ejecuta
     * @param type - Tipo de preferencia que se va a usar
     * @param key - identificador de la prefencia
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setPreferenceI(Context context, int type,String key, Integer value){
        SharedPreferences sharedPreferences = getPreference(context, type);
        sharedPreferences.edit().putInt(key, value).apply();
    }

    /**
     * Guarda el valor de una preferencia de numeros
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setPreferenceI(Context context, String key, Integer value){
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCE_N, context);
        sharedPreferences.edit().putInt(key, value).apply();
    }

    /**
     * Guarda el valor de una preferencia de cadena
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setPreferenceB(Context context, int type, String key, boolean value){
        SharedPreferences sharedPreferences = getPreference(context, type);
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    /**
     * Guarda el valor de una preferencia booleana
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setPreferenceB(Context context, String key, boolean value){
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCE_N, context);
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    /**
     * Guarda una prefencia en formato JSON
     * @param context - en donde se ejecuta
     * @param type - Tipo de preferencia que se va a usar
     * @param value - valor de la prefencia que se va a guardar
     */
    public static void setJSON(Context context, int type, String value){
        SharedPreferences sharedPreferences = getPreference(context, type);
        sharedPreferences.edit().putString(JSON_DATA, value).apply();
    }

    /**
     * Obtiene el valor de una preferencia de cadena
     * @param context - en donde se ejecuta
     * @param type - Tipo de preferencia que se va a usar
     * @param key - identificador de la prefencia
     * @return - el contenido de la prefencia
     */
    public static String getPreferenceS(Context context, int type, String key) {
        SharedPreferences sharedPreferences = getPreference(context, type);
        return sharedPreferences.getString(key, "");
    }

    /**
     * Obtiene el valor de una preferencia de cadena
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @return el contenido de la prefencia
     */
    public static String getPreferenceS(Context context, String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCE_N, context);
        return sharedPreferences.getString(key, "");
    }

    /**
     * Obtiene el valor de una preferencia booleana
     * @param context - en donde se ejecuta
     * @param type - Tipo de preferencia que se va a usar
     * @param key - identificador de la prefencia
     * @return - el contenido de la prefencia
     */
    public static boolean getPreferenceB(Context context, int type, String key) {
        SharedPreferences sharedPreferences = getPreference(context, type);
        return sharedPreferences.getBoolean(key, false);
    }

    /**
     * Obtiene el valor de una preferencia booleana a partir del contexto y la key
     * @param context - en donde se ejecuta
     * @param key - identificador de la prefencia
     * @return - el contenido de la prefencia
     */
    public static boolean getPreferenceB(Context context, String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCE_N, context);
        return sharedPreferences.getBoolean(key, false);
    }

    /**
     * Obtiene una prefencia en formato JSON
     * @param context - En donde se ejecuta
     * @return - el contenido de la prefencia
     */
    public static String getJSON(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCE_N, context);
        return sharedPreferences.getString(JSON_DATA, "");
    }

    /**
     * Inicializa el SharedPrefence
     * @param context - En donde se ejecuta
     * @return SharedPreference
     */
    private static SharedPreferences getSharedPreferences(String preference, Context context) {
        return context.getSharedPreferences(preference, Context.MODE_PRIVATE);
    }

    /**
     * Obtiene y retorna el tipo de preferencia a usar
     * @param context - En donde se ejecuta
     * @param type - tipo de preferencia que se desea usar
     * @return - SharedPreference
     */
    private static SharedPreferences getPreference(Context context, int type){
        SharedPreferences sharedPreferences;
        switch (type) {
            case TYPE_STANDARD:
                sharedPreferences = getSharedPreferences(PREFERENCE_N, context);
                break;
            case TYPE_REGISTER:
                sharedPreferences = getSharedPreferences(PREFERENCE_R, context);
                break;
            case TYPE_ADITIONAL:
                sharedPreferences = getSharedPreferences(PREFERENCE_A, context);
                break;
            case TYPE_FIREBASE:
                sharedPreferences = getSharedPreferences(PREFERENCE_F, context);
                break;
            case TYPE_TEMPORAL:
                sharedPreferences = getSharedPreferences(PREFERENCE_T, context);
                break;
            default:
                sharedPreferences = getSharedPreferences(PREFERENCE_N, context);
                break;
        }
        return sharedPreferences;
    }

    /**
     * Elimina todos los datos de la preferencia seleccionada
     * @param context - En donde se ejecuta
     */
    public static void clearData(Context context, int type){
        SharedPreferences sharedPreferences = getPreference(context, type);
        sharedPreferences.edit().clear().apply();
    }

}